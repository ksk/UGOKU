import path from 'path';

export default {
    plugins: {
        autoprefixer: {},
        'postcss-nested': {},
        'postcss-import': {},
        'postcss-url': {
            url: 'copy',
            assetsPath: path.join(import.meta.dirname, 'target/public/'),
            useHash: true,
        },
    }
};
