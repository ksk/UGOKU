;; SPDX-FileCopyrightText: Copyright 2024 ksk
;; SPDX-License-Identifier: MPL-2.0

(ns ugoku.macros
  (:require
   [helix.core]))

;; https://cljdoc.org/d/lilactown/helix/0.2.0/doc/pro-tips
(defmacro defnc [type & form-body]
  (let [[docstring form-body] (if (string? (first form-body))
                                [(first form-body) (rest form-body)]
                                [nil form-body])
        [fn-meta form-body] (if (map? (first form-body))
                              [(first form-body) (rest form-body)]
                              [nil form-body])
        params (first form-body)
        body (rest form-body)
        opts-map? (map? (first body))
        opts (cond-> (if opts-map?
                       (first body)
                       {})
               (:wrap fn-meta) (assoc :wrap (:wrap fn-meta)))
        default-opts {:helix/features {:fast-refresh true}}]
    `(helix.core/defnc ~type
       ~@(when docstring [docstring])
       ~@(when fn-meta [fn-meta])
       ~params
       ~(merge default-opts opts)
       ~@body)))
