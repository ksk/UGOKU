;; SPDX-FileCopyrightText: Copyright 2024 ksk
;; SPDX-License-Identifier: MPL-2.0

(ns ugoku.three
  (:require
   ["three" :as THREE]
   ["three/addons/controls/OrbitControls.js" :as THREEOrbitControls]
   ["three/addons/helpers/ViewHelper.js" :as THREEViewHelper]))

(defn make-context
  "Makes a new three.js context."
  [canvas]
  (let [renderer (THREE/WebGLRenderer.
                  #js {:canvas canvas
                       :antialias true})
        scene (THREE/Scene.)
        camera (THREE/PerspectiveCamera. 75 1 0.1 1000)
        clock (THREE/Clock.)]
    (.setPixelRatio renderer (.-devicePixelRatio js/window))
    (atom {:canvas canvas
           :renderer renderer
           :scene scene
           :camera camera
           :clock clock
           :update-cb []
           :udata {}})))

(defn add-update-hook
  "Adds an update callback to ctx."
  [ctx cb]
  (swap! ctx update-in [:update-cb] conj cb))

(defn make-camera-controls
  "Adds camera controls to ctx."
  [ctx]
  (let [{canvas :canvas
         camera :camera} @ctx
        controls (THREEOrbitControls/OrbitControls. camera canvas)]
    (set! (.-enableDamping controls) true)
    (set! (.-dampingFactor controls) 0.1)
    (add-update-hook ctx #(if (not (get-in @ctx [:udata :block-controls-update]))
                            (.update controls %2)
                            (swap! ctx update-in [:udata :block-controls-update] (constantly false))))

    controls))

(defn make-view-helper
  "Adds a view helper to ctx."
  [ctx]
  (let [{canvas :canvas
         renderer :renderer
         camera :camera} @ctx
        helper (THREEViewHelper/ViewHelper. camera canvas)]
    (set! (.-autoClear renderer) false)
    (add-update-hook
     ctx
     (fn [_ delta]
       (.clear renderer)
       (if (.-animating helper)
         (do
           (swap! ctx update-in [:udata :block-controls-update] (constantly true))
           (.update helper delta)))
       (.render helper renderer)))
    (.addEventListener canvas "pointerup" #(.handleClick helper %))
    helper))

(defn make-grid
  "Adds a grid with given size and divisions to ctx."
  [ctx size divisions]
  (let [{scene :scene} @ctx
        grid (THREE/GridHelper. size divisions "#eee" "#888")]
    (.add scene grid)
    grid))

(defn update-size
  "Updates the renderer size for ctx."
  ([ctx] (update-size ctx false))
  ([ctx force]
   (let [{canvas :canvas
          renderer :renderer
          ^THREE/PerspectiveCamera camera :camera} @ctx
         width (.-clientWidth canvas)
         height (.-clientHeight canvas)]
     (if (or (not= (.-width canvas) width)
             (not= (.-height canvas) height))
       (do
         (.setSize renderer width height false)
         (set! (.-aspect camera) (/ width height))
         (.updateProjectionMatrix camera))))))

(defn render-main
  "Renders the scene in ctx."
  [ctx]
  (let [{renderer :renderer
         scene :scene
         camera :camera} @ctx]
    (.render renderer scene camera)))

(defn begin-animate
  "Begins a requestAnimationFrame loop which supplies ctx and the frame delta in
  seconds to cb. Returns an atom referring to the last request ID which can be
  used with cancelAnimationFrame to end the loop."
  [ctx]
  (let [anim (atom nil)
        {^THREE/Clock clock :clock
         update-cb :update-cb} @ctx]
    (update-size ctx true)
    (add-update-hook ctx #(update-size %))
    ((fn internal []
       (reset! anim (js/requestAnimationFrame internal))
       (let [delta (.getDelta clock)]
         (doseq [upd update-cb]
           (upd ctx delta)))

       (render-main ctx)))
    anim))
