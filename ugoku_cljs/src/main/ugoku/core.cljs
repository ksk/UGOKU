;; SPDX-FileCopyrightText: Copyright 2024 ksk
;; SPDX-License-Identifier: MPL-2.0

(ns ugoku.core
  (:require
   [helix.core :refer [$]]
   [helix.dom :as d]
   ["react-dom/client" :as rdom]
   ["react-router-dom" :as route]
   [ugoku.layout :refer [layout]]
   [ugoku.main :refer [main]])
  (:require-macros
   [ugoku.macros :refer [defnc]]))

(defonce router
  (route/createBrowserRouter
   (clj->js [{:element ($ layout)
              :children
              [{:path "/"
                :element ($ main)}]}])))

(defn ^:export render []
  (defonce root (rdom/createRoot (js/document.getElementById "app")))
  (.render root ($ route/RouterProvider {:router router})))
