;; SPDX-FileCopyrightText: Copyright 2024 ksk
;; SPDX-License-Identifier: MPL-2.0

(ns ugoku.layout
  (:require
   [helix.core :refer [$]]
   [helix.dom :as d]
   ["react-router-dom" :as route])
  (:require-macros
   [ugoku.macros :refer [defnc]]))

(defnc footer []
  (d/footer
   {:class "footer"}
   (d/img {:class "footer__logo" :src "/logo.svg" :alt "UGOKU logo"})
   (d/nav
    {:class "footer__nav"}
    ($ route/NavLink {:to "/"} "Main")
    (d/a {:href "https://kyo.seki.pw/"} "ksk"))))

(defnc layout []
  (d/main
   ($ route/Outlet)
   (d/div {:style #js {:flexGrow 1}})
   ($ footer)))
