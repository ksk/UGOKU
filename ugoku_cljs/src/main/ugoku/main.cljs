;; SPDX-FileCopyrightText: Copyright 2024 ksk
;; SPDX-License-Identifier: MPL-2.0

(ns ugoku.main
  (:require
   [helix.core :refer [$]]
   [helix.dom :as d]
   [helix.hooks :refer [use-effect]]
   ["three" :as THREE]
   [ugoku.three :as three])
  (:require-macros
   [ugoku.macros :refer [defnc]]))

(defn viewport-animate [ctx delta])

(defn viewport-init []
  (let [canvas (js/document.getElementById "viewport")
        ctx (three/make-context canvas)
        {camera :camera} @ctx]
    (.set (.-position camera) 0 1 5)
    (three/make-camera-controls ctx)
    (three/make-view-helper ctx)
    (three/make-grid ctx 16 32)

    (three/add-update-hook ctx viewport-animate)
    (three/begin-animate ctx)))

(defnc main []
  (use-effect
   :once
   (let [anim (viewport-init)]
     #(js/cancelAnimationFrame @anim)))

  (d/div
   {:class "viewport-container"}
   (d/canvas
    {:id "viewport"
     :style {:width "100%" :height "100vh"}})))
