(defproject pw.seki/ugoku "1.0.0-SNAPSHOT"
  :plugins
  [[cider/cider-nrepl "0.45.0"]]

  :dependencies
  []

  :profiles {:cljs {:source-paths ["src/main"]
                    :dependencies
                    [[thheller/shadow-cljs "2.27.5"]
                     [lilactown/helix "0.2.0"]]}})
