#!/usr/bin/env bash

if [ -d ".venv" ]; then
    exit 0
fi

echo "Setting up Python venv..."

python3 -m venv .venv
source .venv/bin/activate

pip install pymcuprog
