{ pkgs ? import <nixpkgs> {} }:

let
  cwd = builtins.toString ./.;
in pkgs.mkShell {
  buildInputs = with pkgs; [
    # ClojureScript
    nodejs_22

    # PlatformIO
    python312
    platformio
  ];

  shellHook = ''
    ${cwd}/setup_venv.sh
    source ${cwd}/.venv/bin/activate
  '';
}
