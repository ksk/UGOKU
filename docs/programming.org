* Programming

The sensor daughter board is driven by an [[https://www.microchip.com/en-us/product/ATTINY1616][ATtiny1616]], which uses UPDI for programming. A regular serial adapter (e.g. [[https://www.amazon.com/USB-Convert-TTL-Multifunctional-Functions/dp/B01CNW061U][this one]]) can be used to program the MCU using [[https://github.com/microchip-pic-avr-tools/pymcuprog][pymcuprog]] and the guidance provided [[https://github.com/SpenceKonde/AVR-Guidance/blob/master/UPDI/jtag2updi.md][here]].

According to the ATtiny1616 datasheet, UPDI is always pulled up to VCC. It suffices to place a Schottky diode facing towards TX and before the junction between TX and RX.

To test the programming circuit, use =pymcuprog ping=, e.g.

#+begin_src sh
$ pymcuprog -t uart ping -u /dev/ttyUSB0 -d attiny1616
Connecting to SerialUPDI
Pinging device...
Ping response: 1E9421
Done.
#+end_src
