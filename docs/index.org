* UGOKU Documentation

- [[file:tracking.org::*Tracking procedure][Tracking procedure]]
- [[file:imu_model.org::*IMU and magnetometer model][IMU and magnetometer model]]
- [[file:camera_model.org::*Camera model][Camera model]]
- [[file:allan_var.org::*Allan deviation stochastic noise analysis][Allan deviation stochastic noise analysis]]
- [[file:parts.org::*Parts][Parts]]
- [[file:programming.org][Programming]]
