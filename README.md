# UGOKU

Open-source motion capture solution

## Documentation

A documentation explaining the technical details of this project is at `docs/`.

## Similar projects

- [kyoseki (tracker)](https://codeberg.org/voided/tracker), the predecessor to
  this project
- [Bewegungsfelder](https://github.com/herzig/bewegungsfelder), inspiration for
  the original

## Assets

Assets are placed in `assets/`. .vroid and .vrm models are copyright pixiv Inc.
